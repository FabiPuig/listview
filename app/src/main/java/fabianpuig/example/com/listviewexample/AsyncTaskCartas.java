package fabianpuig.example.com.listviewexample;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

/**
 * Created by Fabian on 27/02/2018.
 */

public class AsyncTaskCartas extends AsyncTask<Void, Void, String> {

    private String url;
    private AsyncResponse response;

    public interface AsyncResponse {
        void processFinish( String res );
    }

    // interfaz para extraer la informacion cuando termine
    public AsyncTaskCartas( String url, AsyncResponse response ) {
        this.url = url;
        this.response = response;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... voids) {

        try {
            return HttpUtils.get( url );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {

        // extraemos la informacion mediante la interfaz
        response.processFinish( s );

    }
}
