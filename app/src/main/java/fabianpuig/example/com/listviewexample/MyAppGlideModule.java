package fabianpuig.example.com.listviewexample;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by Fabian on 28/02/2018.
 */

@GlideModule
public class MyAppGlideModule extends AppGlideModule {
}
