package fabianpuig.example.com.listviewexample;

import java.io.Serializable;

/**
 * Created by Fabian on 27/02/2018.
 */

public class Carta implements Serializable{

    private String name;
    private String[] colors;
    private String type;
    private String rarity;
    private String imageUrl;

    public Carta() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getColors() {
        return colors;
    }

    public void setColors(String[] colors) {
        this.colors = colors;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {

        StringBuilder c = new StringBuilder( "* ");
        for (int i = 0; i < colors.length; i++) {

            c.append( colors[i] );
            if( i < colors.length ){

                c.append( " * " );

            }

        }
        return "Carta: " + name + " | " + c.toString() + " | " + type + " | " + rarity;
    }
}
