package fabianpuig.example.com.listviewexample;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetalleActivityFragment extends Fragment {

    private TextView tvName;
    private TextView tvType;
    private TextView tvRarity;
    private ImageView ivCarta;
    private Carta carta;
    private ListView listView;

    public DetalleActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detalle, container, false);

        tvName = view.findViewById( R.id.tv_name );
        tvType = view.findViewById( R.id.tv_type );
        tvRarity = view.findViewById( R.id.tv_rarity );
        ivCarta = view.findViewById( R.id.iv_carta );
        listView = view.findViewById( R.id.lv_colors );

        Intent intent = getActivity().getIntent();
        if( intent != null ){
            carta = (Carta) intent.getSerializableExtra( "carta" );
            if( carta != null ){
                updateUI();
            }
        }

        return view;
    }

    private void updateUI(){

        tvName.setText( carta.getName() );
        tvType.setText( carta.getType() );
        tvRarity.setText( carta.getRarity() );

        GlideApp.with( getContext() )
                .load( carta.getImageUrl() )
                .error( R.drawable.error_image )
                .into( ivCarta );

        ColorsAdapter adapter = new ColorsAdapter( getContext(), R.layout.lv_colors_row, carta.getColors() );
        listView.setAdapter( adapter );

    }
}
