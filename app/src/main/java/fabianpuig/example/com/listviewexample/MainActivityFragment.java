package fabianpuig.example.com.listviewexample;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    //private String url = "https://api.magicthegathering.io/v1/cards";
    private String url = "https://api.magicthegathering.io/v1/cards?colors=red,white,blue";
    private ArrayList<Carta> cartaArrayList;
    private CartaAdapter adapter;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Paso 1: eliminar menu el MainActivity
        //Paso 2: indicamos al fragment que tiene menu
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ListView listView = view.findViewById( R.id.listview );

        // inicializamos el ArrayList
        cartaArrayList = new ArrayList<>();

        // inicializamos el adapter
        adapter = new CartaAdapter( getContext(), R.layout.lv_row, cartaArrayList);

        // Indicamos el adapter a usar por el ListView
        listView.setAdapter( adapter );

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Carta carta = (Carta) adapterView.getItemAtPosition( i );
                Intent intent = new Intent( getContext(), DetalleActivity.class );
                intent.putExtra( "carta", carta );
                startActivity( intent );
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_download) {

            callAsyncTask();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void callAsyncTask(){

        new AsyncTaskCartas(url, new AsyncTaskCartas.AsyncResponse() {
            @Override
            public void processFinish(String res) {

                try {
                    JSONObject object = new JSONObject( res );
                    JSONArray jsonArrayCartas = object.getJSONArray( "cards" );

                    for (int i = 0; i < jsonArrayCartas.length(); i++) {

                        JSONObject o = jsonArrayCartas.getJSONObject( i );

                        Carta c = new Carta();

                        // comentar desde aqui...
                        c.setName( o.optString( "name" ) );
                        c.setRarity( o.optString( "rarity" ) );
                        c.setType( o.optString( "type" ) );
                        c.setImageUrl( o.optString( "imageUrl" ) );

                        JSONArray a = o.getJSONArray( "colors" );

                        ArrayList<String> al = new ArrayList<>();
                        for (int j = 0; j < a.length() ; j++) {
                            al.add( a.getString( j ) );
                        }
                        String[] colores = new String[ al.size() ];
                        al.toArray( colores );
                        c.setColors( colores );

                        // ...hasta aqui para testear json

                        /*
                        // descomentar para testear json
                        // añadir en dependencias: compile 'com.google.code.gson:gson:2.8.2'
                        GsonBuilder builder = new GsonBuilder();
                        Gson gson = builder.create();
                        c = gson.fromJson( o.toString(), Carta.class );

                        Log.i("cartas", c.toString() );

                        */

                        cartaArrayList.add( c );
                        //adapter.add( c );
                    }
                    adapter.addAll( cartaArrayList );


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute();

    }
}
