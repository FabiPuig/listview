package fabianpuig.example.com.listviewexample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Fabian on 27/02/2018.
 */

public class CartaAdapter extends ArrayAdapter<Carta> {

    public CartaAdapter(@NonNull Context context, int resource, @NonNull List<Carta> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        //obtiene la carta de la posicion que muestra
        Carta c = getItem(position);

        // si la vista es nula, infla una nueva vista
        if( convertView == null ){

            LayoutInflater inflater = LayoutInflater.from( getContext() );
            convertView = inflater.inflate( R.layout.lv_row, parent, false );
        }

        ImageView ivImage = convertView.findViewById( R.id.iv_carta );
        TextView tvName = convertView.findViewById( R.id.tv_name );
        TextView tvType = convertView.findViewById( R.id.tv_type );
        TextView tvRarity = convertView.findViewById( R.id.tv_rarity );

        // mostramos los textos deseados
        if( c != null){
            tvName.setText( c.getName() );
            tvType.setText( c.getType() );
            tvRarity.setText( c.getRarity() );

            GlideApp.with( getContext() )
                    .load( c.getImageUrl() )
                    .into( ivImage );
        }

        return convertView;
    }


}
