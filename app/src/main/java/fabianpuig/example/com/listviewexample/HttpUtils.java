package fabianpuig.example.com.listviewexample;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Fabian on 27/02/2018.
 */

public class HttpUtils {

    public static String get(String dataUrl) throws IOException {

        URL url = new URL(dataUrl);
        String respuesta = null;

        HttpURLConnection conectionURL = (HttpURLConnection) url.openConnection();

        try{
            InputStream in = new BufferedInputStream(conectionURL.getInputStream());
            respuesta = readStream(in);
        }finally {
            conectionURL.disconnect();
        }

        return respuesta;
    }

    private static String readStream(InputStream in) throws IOException {

        InputStreamReader isr = new InputStreamReader( in );
        BufferedReader br = new BufferedReader(isr);
        String linea;
        StringBuilder respuesta = new StringBuilder();
        while( (linea = br.readLine() ) != null ){
            respuesta.append(linea);
            respuesta.append("\r");
        }
        br.close();
        return respuesta.toString();
    }

}
