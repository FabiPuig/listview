package fabianpuig.example.com.listviewexample;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by Fabian on 28/02/2018.
 */

public class ColorsAdapter extends ArrayAdapter<String> {

    public ColorsAdapter(@NonNull Context context, int resource, @NonNull String[] objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String color = getItem( position );

        if( convertView == null ){
            LayoutInflater inflater = LayoutInflater.from( getContext() );
            convertView = inflater.inflate( R.layout.lv_colors_row, parent, false );
        }
        TextView tvColor = convertView.findViewById( R.id.tv_color );
        tvColor.setText( color );
        RelativeLayout root = convertView.findViewById( R.id.root );
        if( position % 2 == 0 ){
            tvColor.setBackgroundColor( getContext().getResources().getColor( R.color.white_color ) );
        }else{
            tvColor.setBackgroundColor( getContext().getResources().getColor( R.color.gray_color ) );
        }

        return convertView;
    }
}
